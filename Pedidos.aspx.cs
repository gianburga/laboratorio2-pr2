﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Pedidos : System.Web.UI.Page
{
    private static List<Pedido> pedidos = new List<Pedido>();
    private List<Cliente> clientes;
    private void cargar_clientes()
    {
        DropDownList1.Items.Clear();
        foreach(Cliente cliente in clientes){
            DropDownList1.Items.Add(cliente.Nombre);
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["clientes"] != null)
        {
            clientes = (List<Cliente>)Session["clientes"];
        }
        else
        {
            clientes = new List<Cliente>();
        }   
        if(!IsPostBack){
                     
        }
        cargar_clientes();
        listar_pedidos();
    }
    private void listar_pedidos()
    {
        BulletedList1.Items.Clear();
        foreach(Pedido pedido in pedidos){
            BulletedList1.Items.Add(pedido.FechaRecibido + "("+ pedido.Precio +") - " + pedido.Cliente.Nombre + "(" + pedido.Cliente.GetType() + ")");
        }
    }
    private void limpiar_campos()
    {
        TextBox1.Text = "";
        TextBox3.Text = "";
        TextBox4.Text = "";
        CheckBox1.Checked = false;
    }
    private Cliente buscar_cliente(String nombre)
    {
        foreach(Cliente cliente in clientes){
            if(cliente.Nombre.Equals(nombre)){
                return cliente;
            }
        }
        return null;
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        String fechaRecibido = TextBox1.Text;
        Boolean prepagado = CheckBox1.Checked;
        Double precio = Convert.ToDouble(TextBox4.Text);
        int cantidad = Convert.ToInt32(TextBox3.Text);
        Pedido pedido = new Pedido();
        pedido.FechaRecibido = fechaRecibido;
        pedido.Prepagado = prepagado;
        pedido.Precio = precio;
        pedido.Cantidad = cantidad;
        pedido.Cliente = buscar_cliente(DropDownList1.SelectedItem.ToString());
        pedidos.Add(pedido);
        limpiar_campos();
        listar_pedidos();
    }
}
