﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Clientes : System.Web.UI.Page
{
    static List<Cliente> clientes = new List<Cliente>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack){
            TextBox3.Enabled = false;
            TextBox4.Enabled = false;
            TextBox5.Enabled = false;
            TextBox6.Enabled = false;
            RequiredFieldValidator3.Enabled = false;
            RequiredFieldValidator4.Enabled = false;
            RequiredFieldValidator5.Enabled = false;
            RequiredFieldValidator6.Enabled = false;
        }
        Session["clientes"] = clientes; 
        cargar_clientes();
    }
    private void limpiar_campos()
    {
        TextBox1.Text = "";
        TextBox2.Text = "";
        TextBox3.Text = "";
        TextBox4.Text = "";
        TextBox5.Text = "";
        TextBox6.Text = "";
        RadioButton1.Checked = false;
        RadioButton2.Checked = false;
    }
    private void cargar_clientes()
    {
        BulletedList1.Items.Clear();
        foreach (Cliente cliente in clientes)
        {
            BulletedList1.Items.Add(cliente.Nombre + "(" +cliente.GetType().ToString() + ")");
        }
    }
    protected void RadioButton1_CheckedChanged(object sender, EventArgs e)
    {
        TextBox3.Enabled = true;
        TextBox4.Enabled = true;
        TextBox5.Enabled = true;
        TextBox6.Enabled = false;
        RequiredFieldValidator3.Enabled = true;
        RequiredFieldValidator4.Enabled = true;
        RequiredFieldValidator5.Enabled = true;
        RequiredFieldValidator6.Enabled = false;
    }
    protected void RadioButton2_CheckedChanged(object sender, EventArgs e)
    {
        TextBox3.Enabled = false;
        TextBox4.Enabled = false;
        TextBox5.Enabled = false;
        TextBox6.Enabled = true;
        RequiredFieldValidator3.Enabled = false;
        RequiredFieldValidator4.Enabled = false;
        RequiredFieldValidator5.Enabled = false;
        RequiredFieldValidator6.Enabled = true;
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        String nombre = TextBox1.Text;
        String direccion = TextBox2.Text;
        if(RadioButton1.Checked){
            String contacto = TextBox3.Text;
            String calificacion = TextBox4.Text;
            String limite = TextBox5.Text;
            ClienteCorporativo clienteCorporativo = new ClienteCorporativo();
            clienteCorporativo.Nombre = nombre;
            clienteCorporativo.Direccion = direccion;
            clienteCorporativo.LimiteCredito = limite;
            clienteCorporativo.CalificacionCredito1 = calificacion;
            clienteCorporativo.NombreContacto = contacto;
            clientes.Add(clienteCorporativo);
        }else if(RadioButton2.Checked){
            String tarjeta = TextBox6.Text;
            ClientePersonal clientePersonal = new ClientePersonal();
            clientePersonal.Nombre = nombre;
            clientePersonal.Direccion = direccion;
            clientePersonal.TarjetaCredito = tarjeta;
            clientes.Add(clientePersonal);
        }
        limpiar_campos();
        cargar_clientes();
    }
}
