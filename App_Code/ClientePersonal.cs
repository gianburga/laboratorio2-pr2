﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;

public class ClientePersonal : global::Cliente
{
    private string tarjetaCredito;

    public string TarjetaCredito
    {
        get { return tarjetaCredito; }
        set { tarjetaCredito = value; }
    }
}
