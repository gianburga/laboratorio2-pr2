﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;

public class LineaPedido
{
    private int cantidad;

    public int Cantidad
    {
        get { return cantidad; }
        set { cantidad = value; }
    }
    private int precio;

    public int Precio
    {
        get { return precio; }
        set { precio = value; }
    }
    private int satisfecho;

    public int Satisfecho
    {
        get { return satisfecho; }
        set { satisfecho = value; }
    }

    private Pedido pedido;

    public global::Pedido Pedido
    {
        get
        {
            return pedido;
        }
        set
        {
            pedido = value;
        }
    }
}
