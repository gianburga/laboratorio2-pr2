﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;

public class Cliente
{
    private string nombre;

    public string Nombre
    {
        get { return nombre; }
        set { nombre = value; }
    }
    private String direccion;

    public string Direccion
    {
        get { return direccion; }
        set { direccion = value; }
    }

    public String CalificacionCredito()
    {
        return "";
    }

    public String ToString()
    {
        return Nombre;
    }
}
