﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;

public class Pedido
{
    private string fechaRecibido;

    public string FechaRecibido
    {
        get { return fechaRecibido; }
        set { fechaRecibido = value; }
    }
    private Boolean prepagado;

    public bool Prepagado
    {
        get { return prepagado; }
        set { prepagado = value; }
    }
    private int cantidad;

    public int Cantidad
    {
        get { return cantidad; }
        set { cantidad = value; }
    }
    private double precio;

    public double Precio
    {
        get { return precio; }
        set { precio = value; }
    }

    private Cliente cliente;

    public global::Cliente Cliente
    {
        get
        {
            return cliente;
        }
        set
        {
            cliente = value;
        }
    }
}
