﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;

public class ClienteCorporativo : global::Cliente
{
    private string nombreContacto;

    public string NombreContacto
    {
        get { return nombreContacto; }
        set { nombreContacto = value; }
    }
    private string calificacionCredito;

    public string CalificacionCredito1
    {
        get { return calificacionCredito; }
        set { calificacionCredito = value; }
    }
    private string limiteCredito;

    public string LimiteCredito
    {
        get { return limiteCredito; }
        set { limiteCredito = value; }
    }
}
